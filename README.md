# Clues Message

## About

Created with Unity

## Genre

Educational Game, Visual Storytelling, Casual

## Platform

Android

## Development Team
- Ilham Pratama
- Zsalsabila Pasya E.
- Aditya Nur Juang R.


## Screenshot

![](images/10.PNG)

![](images/9.PNG)

![](images/11.PNG)

![](images/12.PNG)