﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square : MonoBehaviour
{
    // Start is called before the first frame update
    private Touch touch;
    private Collider2D col;
    
    void Start()
    {
        col = GetComponent<Collider2D>();
        GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1 )
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                Collider2D touchedCollider = Physics2D.OverlapPoint(touchPosition);
                if (col == touchedCollider)
                {
                    Debug.Log("square touched");
                    CheckTouch('S');                    
                }
            }

            if (touch.phase == TouchPhase.Moved)
            {

            }

            //when touch ended
            if (touch.phase == TouchPhase.Ended)
            {
                
            }

        }

    }

    public void deleteObjects()
    {
        Destroy(this.gameObject);
    }

    void CheckTouch(char shape)
    {
        if (GameManager.match == ' ')
        {
            //Display 1
            Debug.Log("Square 1");
            GameManager.match = shape;
            GameManager.objects[0] = this.gameObject;
        }

        else if (GameManager.match == shape)
        {
            //Display 2
            Debug.Log("Square 2");
            GameManager.match2 = shape;
            GameManager.objects[1] = this.gameObject;
        }

        else if (GameManager.match != shape)
        {
            Debug.Log("Square 1");
            GameManager.match = shape;
            GameManager.objects[0] = this.gameObject;
        }

        else if (GameManager.match2 != shape)
        {
            Debug.Log("Square 2");
            GameManager.match2 = shape;
            GameManager.objects[1] = this.gameObject;
        }

        else
        {
            GameManager.match = shape;
            GameManager.objects[0] = this.gameObject;
        }
    }
}
