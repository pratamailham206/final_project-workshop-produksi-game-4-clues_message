﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static char match;
    public static char match2;

    private int NextLevel;
    public static GameObject[] objects = new GameObject[2];
    public GameObject particle;

    public RewardScript reward;
    void Start()
    {
        match = ' ';
        match2 = ' ';
        NextLevel = 0;
        GetComponent<MailScript>();
        GetComponent<CharacterStatusScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (match == 'S' && match2 == 'S') {
            Debug.Log("Match Square");
            match = ' ';
            match2 = ' ';
            objects[0].GetComponent<Square>().deleteObjects();
            objects[1].GetComponent<Square>().deleteObjects();
            Instantiate(particle, new Vector3(objects[0].transform.position.x, objects[0].transform.position.y), Quaternion.identity);
            Instantiate(particle, new Vector3(objects[1].transform.position.x, objects[1].transform.position.y), Quaternion.identity);
            objects[0] = null;
            objects[1] = null;
            NextLevel += 1;
            
        }

        if (match == 'D' && match2 == 'D')
        {
            Debug.Log("Match Diamond");
            match = ' ';
            match2 = ' ';
            objects[0].GetComponent<Diamond>().deleteObjects();
            objects[1].GetComponent<Diamond>().deleteObjects();
            Instantiate(particle, new Vector3(objects[0].transform.position.x, objects[0].transform.position.y), Quaternion.identity);
            Instantiate(particle, new Vector3(objects[1].transform.position.x, objects[1].transform.position.y), Quaternion.identity);
            objects[0] = null;
            objects[1] = null;           
            NextLevel += 1;
            
        }

        if (match == 'C' && match2 == 'C')
        {
            Debug.Log("Match Circle");
            match = ' ';
            match2 = ' ';
            objects[0].GetComponent<Circle>().deleteObjects();
            objects[1].GetComponent<Circle>().deleteObjects();
            Instantiate(particle, new Vector3(objects[0].transform.position.x, objects[0].transform.position.y), Quaternion.identity);
            Instantiate(particle, new Vector3(objects[1].transform.position.x, objects[1].transform.position.y), Quaternion.identity);
            objects[0] = null;
            objects[1] = null;
            NextLevel += 1;
            
        }

        if (match == 'T' && match2 == 'T')
        {
            Debug.Log("Match Triangle");
            match = ' ';
            match2 = ' ';
            objects[0].GetComponent<Triangle>().deleteObjects();
            objects[1].GetComponent<Triangle>().deleteObjects();
            Instantiate(particle, new Vector3(objects[0].transform.position.x, objects[0].transform.position.y), Quaternion.identity);
            Instantiate(particle, new Vector3(objects[1].transform.position.x, objects[1].transform.position.y), Quaternion.identity);
            objects[0] = null;
            objects[1] = null;
            NextLevel += 1;
            
        }

        if(NextLevel >= 3 )
        {
            //WIN CONDITION
            MailScript.Minigame2Complete = true;

            //SHOW MODAL
            reward.setModal(100, 10, 25, 100);
            reward.rewardOpen();

            //INCREASE STATUS
            CharacterStatusScript.exp += 100;
            CharacterStatusScript.gold += 100;
            CharacterStatusScript.trust += 10;
            CharacterStatusScript.intel += 25;
        }
    }
}
