﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RewardScript : MonoBehaviour
{
    public GameObject modal;

    //STATUS
    int goldRec;
    int trustRec;
    int intelRec;
    int expRec;

    public Text goldT;
    public Text trustT;
    public Text intelT;

    void Start()
    {
        modal.SetActive(false);
        goldRec = 0;
        trustRec = 0;
        intelRec = 0;
        expRec = 0;
    }

    void Update()
    {
        goldT.GetComponent<Text>().text = goldRec.ToString();
        trustT.GetComponent<Text>().text = trustRec.ToString();
        intelT.GetComponent<Text>().text = intelRec.ToString();
    }

    public void rewardOpen()
    {
        modal.SetActive(true);
    }

    public void setModal(int g, int t, int i, int e)
    {
        this.goldRec = g;
        this.trustRec = t;
        this.intelRec = i;
        this.expRec = e;
    }

    public void backToMenu()
    {
        SceneManager.LoadScene("MainGame");
    }

    public void backToMenu1()
    {
        SceneManager.LoadScene("DS-mini1-epi");
    }
    public void backToMenu2()
    {
        SceneManager.LoadScene("DS-mini2-epi");
    }
    public void backToMenu3()
    {
        SceneManager.LoadScene("DS-mini3-epi");
    }

    public void backToMenu5()
    {
        SceneManager.LoadScene("DS-mini5-epi");
    }

}
