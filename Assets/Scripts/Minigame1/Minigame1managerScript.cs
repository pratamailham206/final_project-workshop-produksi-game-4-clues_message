﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minigame1managerScript : MonoBehaviour
{
    // Start is called before the first frame update
    public RewardScript reward;

    void Start()
    {
        GetComponent<trashSpawner>();
        GetComponent<MailScript>();
        GetComponent<CharacterStatusScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (trashSpawner.littered >= 12)
        {
            Debug.Log("All Trash has been ");
            MailScript.Minigame1Complete = true;

            //SHOW MODAL
            reward.setModal(101, 11, 21, 101);
            reward.rewardOpen();

            //INCREASE STATUS
            CharacterStatusScript.exp += 101;
            CharacterStatusScript.gold += 101;
            CharacterStatusScript.trust += 11;
            CharacterStatusScript.intel += 21;
        }
    }
}
