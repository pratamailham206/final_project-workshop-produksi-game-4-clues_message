﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class trashSpawner : MonoBehaviour
{
    public GameObject[] trashType;
    public static int littered;

    // Start is called before the first frame update
    void Start()
    {
        littered = 0;
        int rand = Random.Range(0, trashType.Length);
        Instantiate(trashType[rand], transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
