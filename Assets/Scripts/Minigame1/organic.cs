﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class organic : MonoBehaviour
{
    public GameObject particle;
    private void Start()
    {
        GetComponent<trashSpawner>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("organic"))
        {
            Instantiate(particle, new Vector3(this.transform.position.x, this.transform.position.y), Quaternion.identity);
            Destroy(gameObject);
            trashSpawner.littered++;
        }
    }
}
