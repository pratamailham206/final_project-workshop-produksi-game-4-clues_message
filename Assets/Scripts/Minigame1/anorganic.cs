﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anorganic : MonoBehaviour
{
    public GameObject particle;
    private void Start()
    {
        GetComponent<trashSpawner>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("anorganic"))
        {
            Instantiate(particle, new Vector3(this.transform.position.x, this.transform.position.y), Quaternion.identity);
            Instantiate(particle);
            Destroy(gameObject);
            trashSpawner.littered++;
        }
    }
}
