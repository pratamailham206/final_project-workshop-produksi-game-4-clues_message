﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceNumberScript : MonoBehaviour
{
    public Transform numberPlace;

    private Vector2 initialPosition;

    private float deltaX, deltaY;
    void Start()
    {
        initialPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
                    {
                        deltaX = touchPos.x - transform.position.x;
                        deltaY = touchPos.y - transform.position.y;
                    }
                    break;
                case TouchPhase.Moved:
                    if (GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
                    {
                        transform.position = new Vector2(touchPos.x - deltaX, touchPos.y - deltaY);
                    }
                    break;
                case TouchPhase.Ended:
                    if(Mathf.Abs(transform.position.x - numberPlace.position.x) <= 2f &&
                        Mathf.Abs(transform.position.y - numberPlace.position.y) <= 2f)
                    {
                        transform.position = new Vector2(numberPlace.position.x, numberPlace.position.y);
                    }
                    else
                    {
                        transform.position = new Vector2(initialPosition.x, initialPosition.y);
                    }
                    break;
            }
        }
    }
}
