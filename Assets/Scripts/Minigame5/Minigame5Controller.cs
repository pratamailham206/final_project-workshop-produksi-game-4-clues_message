﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Minigame5Controller : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] question;
    public GameObject nextButton;
    public GameObject finishButton;

    public RewardScript reward;

    private int list;
    private int i;
    public static int corrects;
    void Start()
    {
        question[0].SetActive(true);
        for (int i = 1; i < 5; i++)
        {
            question[i].SetActive(false);
        }
        list = 0;
        i = -1;
        corrects = 0;
        finishButton.SetActive(false);
        GetComponent<CharacterStatusScript>();
        GetComponent<MailScript>();        
    }

    // Update is called once per frame
    void Update()
    {
        if(list >= 4)
        {
            Destroy(nextButton);
            finishButton.SetActive(true);
        }
    }

    public void nextQuestion()
    {
        list += 1;
        i += 1;
        Destroy(question[i]);
        question[list].SetActive(true);        
        Debug.Log("increment : " + list);
    }

    public void FinishQuest()
    {
        //SceneManager.LoadScene(0);
        MailScript.Minigame4Complete = true;

        //SHOW MODAL
        reward.setModal(100, 10, 25, 100);
        reward.rewardOpen();

        //INCREASE STATUS
        CharacterStatusScript.exp += 100;
        CharacterStatusScript.gold += 100;
        CharacterStatusScript.trust += 10;
        CharacterStatusScript.intel += 25;
    }
}
