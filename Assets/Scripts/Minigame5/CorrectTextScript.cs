﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CorrectTextScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Text text;
    void Start()
    {
        GetComponent<Minigame5Controller>();
    }

    // Update is called once per frame
    void Update()
    {
        text.GetComponent<Text>().text = Minigame5Controller.corrects.ToString();
    }
}
