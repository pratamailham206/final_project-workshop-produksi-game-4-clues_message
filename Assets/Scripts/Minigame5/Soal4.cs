﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soal4 : MonoBehaviour
{
    public GameObject particle;
    void Start()
    {
        GetComponent<Minigame5Controller>();
        this.gameObject.GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("3"))
        {
            Minigame5Controller.corrects++;
            Instantiate(particle, new Vector3(this.transform.position.x, this.transform.position.y), Quaternion.identity);
            Debug.Log("Benar");
        }

        else
        {
            Debug.Log("Wrong Number");
        }
    }
}
