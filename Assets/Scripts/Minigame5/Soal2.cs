﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soal2 : MonoBehaviour
{
    public GameObject particle;
    void Start()
    {
        GetComponent<Minigame5Controller>();
        this.gameObject.GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("4"))
        {
            Minigame5Controller.corrects++;
            Debug.Log("Benar");
            Instantiate(particle, new Vector3(this.transform.position.x, this.transform.position.y), Quaternion.identity);
        }

        else
        {
            Debug.Log("Wrong Number");
        }
    }
}
