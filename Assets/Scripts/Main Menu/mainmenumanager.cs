﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class mainmenumanager : MonoBehaviour
{
    public GameObject main, setting, credit, exit;
    public bool onMusic, onSFX;
    public static bool introDS = false;

    void Start()
    {
        GetComponent<DialogScript>();
        FindObjectOfType<audiomanager>().Play("BGM");
        onMusic = true;
        onSFX= true;
        main.SetActive(true);
        setting.SetActive(false);
        credit.SetActive(false);
        exit.SetActive(false);
        DialogScript.prolog = true;
    }

    public void starGame()
    {
        if (onSFX == true)
        {
            FindObjectOfType<audiomanager>().Play("SFX");
            if (introDS)
            {
                SceneManager.LoadScene("MainGame");
                FindObjectOfType<audiomanager>().Stop("BGM");
                FindObjectOfType<audiomanager>().Play("Menu");
            }
            else
            {
                SceneManager.LoadScene("DS-Intro");
                FindObjectOfType<audiomanager>().Stop("BGM");
                FindObjectOfType<audiomanager>().Play("Menu");
            }

        }
        else
        {
            if (introDS)
            {
                SceneManager.LoadScene("MainGame");
                FindObjectOfType<audiomanager>().Stop("BGM");
                FindObjectOfType<audiomanager>().Play("Menu");
            }
            else
            {
                SceneManager.LoadScene("DS-Intro");
                FindObjectOfType<audiomanager>().Stop("BGM");
                FindObjectOfType<audiomanager>().Play("Menu");
            }
        }
       
    }

    public void settingGame()
    {
        if (onSFX == true)
        {
            FindObjectOfType<audiomanager>().Play("SFX");
            setting.SetActive(true);
            main.SetActive(false);
            credit.SetActive(false);
            exit.SetActive(false);

        }
        else
        {
            setting.SetActive(true);
            main.SetActive(false);
            credit.SetActive(false);
            exit.SetActive(false);
        }
       
    }

    public void creditsGame()
    {
        if (onSFX == true)
        {
            FindObjectOfType<audiomanager>().Play("SFX");
            setting.SetActive(false);
            main.SetActive(false);
            credit.SetActive(true);
            exit.SetActive(false);

        }
        else
        {
            setting.SetActive(false);
            main.SetActive(false);
            credit.SetActive(true);
            exit.SetActive(false);
        }
       
    }

    public void exitGame()
    {
        if (onSFX == true)
        {
            FindObjectOfType<audiomanager>().Play("SFX");
            exit.SetActive(true);
            main.SetActive(false);
            setting.SetActive(false);
            credit.SetActive(false);

        }
        else
        {
            exit.SetActive(true);
            main.SetActive(false);
            setting.SetActive(false);
            credit.SetActive(false);
        }
    }

    public void backMain()
    {
        if (onSFX == true)
        {
            FindObjectOfType<audiomanager>().Play("SFX");
            main.SetActive(true);
            setting.SetActive(false);
            credit.SetActive(false);
            exit.SetActive(false);

        }
        else
        {
            main.SetActive(true);
            setting.SetActive(false);
            credit.SetActive(false);
            exit.SetActive(false);
        }
      
    }

    public void exits()
    {
        if (onSFX == true)
        {
            FindObjectOfType<audiomanager>().Play("SFX");
            Application.Quit();
            Debug.Log("Game Exit");

        }
        else
        {
            Application.Quit();
            Debug.Log("Game Exit");
        }

    }

    public void musicOn()
    {
        if(onMusic == false)
        {
            FindObjectOfType<audiomanager>().Play("BGM");
            onMusic = true;
        }
        else
        {

        }
    }

    public void musicOff()
    {
        if (onMusic == true)
        {
            FindObjectOfType<audiomanager>().Stop("BGM");
            onMusic = false;
        }
        else
        {

        }
    }

    public void sfxOn()
    {
        if (onSFX == false)
        {
            FindObjectOfType<audiomanager>().Play("SFX");
            onSFX = true;
        }
        else
        {

        }
    }

    public void sfxOff()
    {
        if (onSFX == true)
        {
            FindObjectOfType<audiomanager>().Stop("SFX");
            onSFX = false;
        }
        else
        {

        }
    }

}
