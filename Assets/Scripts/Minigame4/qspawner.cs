﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class qspawner : MonoBehaviour
{
    public GameObject q1, q2, q3, q4, q5;
    public float spawnRate;
    float nextSpawn = 0f;
    int whatToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        q1.SetActive(false);
        q2.SetActive(false);
        q3.SetActive(false);
        q4.SetActive(false);
        q5.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextSpawn)
        {
            whatToSpawn = Random.Range(1, 5);
            Debug.Log(whatToSpawn);

            switch (whatToSpawn)
            {
                case 1:
                    q1.SetActive(true);
                    q2.SetActive(false);
                    q3.SetActive(false);
                    q4.SetActive(false);
                    q5.SetActive(false);
                    break;
                case 2:
                    q2.SetActive(true);
                    q1.SetActive(false);
                    q3.SetActive(false);
                    q4.SetActive(false);
                    q5.SetActive(false);
                    break;
                case 3:
                    q3.SetActive(true);
                    q2.SetActive(false);
                    q1.SetActive(false);
                    q4.SetActive(false);
                    q5.SetActive(false);
                    break;
                case 4:
                    q4.SetActive(true);
                    q2.SetActive(false);
                    q3.SetActive(false);
                    q1.SetActive(false);
                    q5.SetActive(false);
                    break;
                case 5:
                    q5.SetActive(true);
                    q2.SetActive(false);
                    q3.SetActive(false);
                    q4.SetActive(false);
                    q1.SetActive(false);
                    break;
            }
            nextSpawn = Time.time + spawnRate;
        }
    }
}
