﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gamemanager : MonoBehaviour
{
    public int point, click;
    public GameObject right, wrong;
    public Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = point.ToString("0");
    }

    public void wrongans()
    {
        if (click < 1)
        {
            wrong.SetActive(true);
            click += 1;
        }
        else
        {
            click = 0;
        }
        
    }

    public void rightans()
    {
        if(click < 1)
        {
            right.SetActive(true);
            point += 1;
            click += 1;
        }
        else
        {
            click = 0;
        }
       
    }

}
