﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject pauseModal;
    void Start()
    {
        pauseModal.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onPauseGame()
    {
        FindObjectOfType<audiomanager>().Play("SFX");
        pauseModal.SetActive(true);
    }

    public void cancelEvent()
    {
        FindObjectOfType<audiomanager>().Play("SFX");
        pauseModal.SetActive(false);
    }

    public void backToMenu()
    {
        FindObjectOfType<audiomanager>().Play("SFX");
        SceneManager.LoadScene("MainGame");
        FindObjectOfType<audiomanager>().Stop("Mini");
        FindObjectOfType<audiomanager>().Play("Game");
    }

    public void RestartGame1()
    {
        FindObjectOfType<audiomanager>().Play("SFX");
        SceneManager.LoadScene("Minigame1");
    }

    public void RestartGame2()
    {
        FindObjectOfType<audiomanager>().Play("SFX");
        SceneManager.LoadScene("Minigame2");
    }

    public void RestartGame3()
    {
        FindObjectOfType<audiomanager>().Play("SFX");
        SceneManager.LoadScene("Minigame3");
    }

    public void RestartGame4()
    {
        FindObjectOfType<audiomanager>().Play("SFX");
        SceneManager.LoadScene("Minigame5");
    }
}
