﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject modal;
    void Start()
    {
        GetComponent<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onOpenModal() {
        if (UIManager.modalActive == false) {
            modal.SetActive(true);
            UIManager.modalActive = true;
        }

        else {
            Debug.Log("close modal first");
        }
        FindObjectOfType<audiomanager>().Play("SFX");
    }

    public void CloseModal() {
        modal.SetActive(false);
        UIManager.modalActive = false;
        FindObjectOfType<audiomanager>().Play("SFX");
    }
}
