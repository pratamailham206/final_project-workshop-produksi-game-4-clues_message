﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatusScript : MonoBehaviour
{
    // Start is called before the first frame update
    public static int level;
    public static int gold;
    public static int intel;
    public static int trust;
    public static int exp;

    public Text levelText;
    public Text goldText;
    public Text intelText;
    public Text trustText;
    public Text expText;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        levelText.GetComponent<Text>().text = level.ToString();
        goldText.GetComponent<Text>().text = gold.ToString();
        intelText.GetComponent<Text>().text = intel.ToString();
        trustText.GetComponent<Text>().text = trust.ToString();
        expText.GetComponent<Text>().text = exp.ToString();
    }
}
