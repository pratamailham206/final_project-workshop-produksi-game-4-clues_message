﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BacktoMenuScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void openBackToMenu() {
        FindObjectOfType<audiomanager>().Play("SFX");
        SceneManager.LoadScene("MainMenuScene");
    }    
}
