﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MailScript : MonoBehaviour
{
    // Start is called before the first frame update
    public static bool Minigame1Complete;
    public static bool Minigame2Complete;
    public static bool Minigame3Complete;
    public static bool Minigame4Complete;

    public GameObject Minigame1CompleteTag;
    public GameObject Minigame2CompleteTag;
    public GameObject Minigame3CompleteTag;
    public GameObject Minigame4CompleteTag;

    void Start()
    {
        this.gameObject.SetActive(false);

        Minigame1CompleteTag.SetActive(false);
        Minigame2CompleteTag.SetActive(false);
        Minigame3CompleteTag.SetActive(false);
        Minigame4CompleteTag.SetActive(false);

        GetComponent<DialogMini1>();
        GetComponent<DialogMini2>();
        GetComponent<DialogMini3>();
        GetComponent<DialogMini5>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Minigame1Complete) {
            Minigame1CompleteTag.SetActive(true);
        }

        if(Minigame2Complete) {
            Minigame2CompleteTag.SetActive(true);
        }

        if(Minigame3Complete) {
            Minigame3CompleteTag.SetActive(true);
        }

        if(Minigame4Complete) {
            Minigame4CompleteTag.SetActive(true);
        }
        
        if(Minigame1Complete && Minigame2Complete && Minigame3Complete && Minigame4Complete)
        {
            SceneManager.LoadScene("DS-Ending");
        }
    }

    public void OpenMinigame1() {
        FindObjectOfType<audiomanager>().Play("SFX");
        DialogMini1.prolog = true;
        SceneManager.LoadScene("DS-mini1-intro");
        FindObjectOfType<audiomanager>().Stop("Game");
        FindObjectOfType<audiomanager>().Play("Mini");
    }

    public void OpenMinigame2() {
        FindObjectOfType<audiomanager>().Play("SFX");
        DialogMini2.prolog = true;
        SceneManager.LoadScene("DS-mini2-intro");
        FindObjectOfType<audiomanager>().Stop("Game");
        FindObjectOfType<audiomanager>().Play("Mini");
    }

    public void OpenMinigame3() {
        FindObjectOfType<audiomanager>().Play("SFX");
        DialogMini3.prolog = true;
        SceneManager.LoadScene("DS-mini3-intro");
        FindObjectOfType<audiomanager>().Stop("Game");
        FindObjectOfType<audiomanager>().Play("Mini");
    }

    public void OpenMinigame4() {
        FindObjectOfType<audiomanager>().Play("SFX");
        DialogMini5.prolog = true;
        SceneManager.LoadScene("DS-mini5-intro");
        FindObjectOfType<audiomanager>().Stop("Game");
        FindObjectOfType<audiomanager>().Play("Mini");
    }
}
