﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingScript : MonoBehaviour
{
    bool onMusic, onSFX;
    void Start()
    {
        onMusic = true;
        onSFX = false;
        this.gameObject.SetActive(false);
    }

    public void musicOn()
    {
        if(onMusic == false)
        {
            FindObjectOfType<audiomanager>().Play("Game");
            onMusic = true;
        }
        else
        {

        }
    }

    public void musicOff()
    {
        if (onMusic == true)
        {
            FindObjectOfType<audiomanager>().Stop("Game");
            onMusic = false;
        }
        else
        {

        }
    }

    public void sfxOn()
    {
        if (onSFX == false)
        {
            FindObjectOfType<audiomanager>().Play("SFX");
            onSFX = true;
        }
        else
        {

        }
    }

    public void sfxOff()
    {
        if (onSFX == true)
        {
            FindObjectOfType<audiomanager>().Stop("SFX");
            onSFX = false;
        }
        else
        {

        }
    }
}
