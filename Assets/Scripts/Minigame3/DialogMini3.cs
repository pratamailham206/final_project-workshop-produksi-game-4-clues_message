﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class DialogMini3 : MonoBehaviour
{
    // Start is called before the first frame update
    public TextMeshProUGUI textDisplay;
    public string[] sentences;
    private int index;
    public float typingSpeed;

    public GameObject continueButton;
    public GameObject changeSceneButton;

    public static bool prolog;

    void Start()
    {
        StartCoroutine(Type());
    }

    // Update is called once per frame
    void Update()
    {
        if (textDisplay.text == sentences[index])
        {
            if (index >= sentences.Length - 1)
            {
                changeSceneButton.SetActive(true);
            }

            else
            {
                continueButton.SetActive(true);
            }
        }
    }

    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void NextSentence()
    {
        continueButton.SetActive(false);
        if (index < sentences.Length - 1)
        {
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }

        else
        {
            textDisplay.text = "";
            continueButton.SetActive(false);
        }
        FindObjectOfType<audiomanager>().Play("SFX");
    }

    public void ChangeScene()
    {
        if (prolog)
        {
            SceneManager.LoadScene("Minigame3");
            prolog = false;
        }

        else if (prolog == false)
        {
            SceneManager.LoadScene("MainGame");
        }
    }
}
