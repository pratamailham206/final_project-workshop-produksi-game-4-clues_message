﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TargetManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static int objectsFound;
    public Text text;
    public RewardScript reward;

    void Start()
    {
        objectsFound = 0;
        GetComponent<MailScript>();
        GetComponent<CharacterStatusScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if(objectsFound == 5)
        {
            //WIN CONDITION
            Debug.Log("All objects has been found");
            MailScript.Minigame3Complete = true;
            //SceneManager.LoadScene(0);

            //SHOW MODAL
            reward.setModal(100, 10, 25, 100);
            reward.rewardOpen();

            //INCREASE STATUS
            CharacterStatusScript.exp += 100;
            CharacterStatusScript.gold += 100;
            CharacterStatusScript.trust += 10;
            CharacterStatusScript.intel += 25;
        }
        text.GetComponent<Text>().text = objectsFound.ToString();
    }
}
