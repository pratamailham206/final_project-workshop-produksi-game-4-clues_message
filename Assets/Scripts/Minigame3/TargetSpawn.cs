﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawn : MonoBehaviour
{
    public Transform[] spawnPos;
    // Start is called before the first frame update
    Collider2D col;
    private bool found;
    public GameObject particle;
    void Start()
    {
        int rand = Random.Range(0, spawnPos.Length);
        this.gameObject.transform.position = spawnPos[rand].position;
        col = GetComponent<Collider2D>();
        GetComponent<TargetManager>();
        found = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
            Collider2D touchedCollider = Physics2D.OverlapPoint(touchPosition);
            if (touch.phase == TouchPhase.Began)
            {                
                if (col == touchedCollider)
                {
                    Debug.Log("Object Found");
                    if(!found)
                    {
                        TargetManager.objectsFound += 1;
                        found = true;
                        Destroy(this.gameObject);
                        Instantiate(particle, new Vector3(this.transform.position.x, this.transform.position.y), Quaternion.identity);
                    }                   
                }
            }

            if (touch.phase == TouchPhase.Moved)
            {

            }

            //when touch ended
            if (touch.phase == TouchPhase.Ended)
            {
                
            }
        }
    }
}
